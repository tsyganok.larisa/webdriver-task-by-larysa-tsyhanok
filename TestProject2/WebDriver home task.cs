using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;


namespace WebDriverHomeTask
{
	public class Tests
	{
		readonly String test_url = "https://www.google.com";

		IWebDriver driver;

		[SetUp]
		public void start_Browser()
		{
			driver = new ChromeDriver();
			driver.Manage().Window.Maximize();			
		}

		[Test]
		public void test_SearchImage()
		{
			driver.Navigate().GoToUrl(test_url);

			var title = driver.Title;
			Assert.That(title, Is.EqualTo("Google"));

			driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

			var searchBox = driver.FindElement(By.Name("q"));
			var searchButton = driver.FindElement(By.Name("btnK"));

			searchBox.SendKeys("manul photos");
			searchButton.Click();

			try
			{
				var result = driver.FindElement(By.ClassName("iJ1Kvb"));
				var value = result.Enabled;
				Assert.That(value);
				Assert.Pass("Test passed");
			}
			catch(NullReferenceException e)
			{
				Assert.Fail(e.Message);
			}			

		}

		[TearDown]
		public void close_Browser()
		{
			driver.Quit();
		}

	}
}